function escapeRegExpChars(string) {
    return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

function extractNumbers(numbers, delimiters) {
    if(!delimiters.length) {
        delimiters = [',', '\n'];
    }

    const escapedDelimiters = delimiters.map(delimiter => escapeRegExpChars(delimiter));
    const regExp = new RegExp(escapedDelimiters.join('|'));
    return numbers.split(regExp).map(number => parseInt(number, 10) || 0);
}

function splitInput(input) {
    const [ignore, delimiter, numbers] = input.match(/^(?:\/\/((?:\[.*])|.)\n)?([\s\S]*)$/);
    return [numbers, delimiter];
}

function getNegativeNumbers(numbers) {
    return numbers.filter(number => number < 0);
}

function removeNumbersBiggerThen1000(numbers) {
    return numbers.filter(number => number <= 1000);
}

function extractDelimiters(delimiters) {
    if(!delimiters) {
        return [];
    }

    const match = delimiters.match(/^\[(.*)]$/);
    if(!match) {
        return [delimiters];
    }

    return match[1].split('][');
}

export class StringCalculator {
    static add(input) {
        const [numbersStr, delimitersStr] = splitInput(input);
        const delimitersArr = extractDelimiters(delimitersStr);
        const numbersArr = extractNumbers(numbersStr, delimitersArr);

        const negativeNumbersArr = getNegativeNumbers(numbersArr);
        if(negativeNumbersArr.length) {
            throw new RangeError(`negatives not allowed: ${negativeNumbersArr.join(', ')}`);
        }

        const filteredNumbersArr = removeNumbersBiggerThen1000(numbersArr);

        return filteredNumbersArr.reduce((number, result) => result + number, 0);
    }
}
