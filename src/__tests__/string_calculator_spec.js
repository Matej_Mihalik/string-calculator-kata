import { StringCalculator } from '../string_calculator';

describe('String Calculator', function() {
    describe('Step 1', function() {
        it('should return 0 for empty string', function() {
            const result = StringCalculator.add('');

            expect(result).toBe(0);
        });

        it('should return the same number for string with one number', function() {
            const result = StringCalculator.add('1');

            expect(result).toBe(1);
        });

        it('should return the sum of the numbers for string with two numbers', function() {
            const result = StringCalculator.add('1,2');

            expect(result).toBe(3);
        });
    });

    describe('Step 2', function() {
        it('should handle any amount of numbers', function() {
            const result = StringCalculator.add('1,2,3');

            expect(result).toBe(6);
        });
    });

    describe('Step 3', function() {
        it('should handle new line delimiter', function() {
            const result = StringCalculator.add('1\n2');

            expect(result).toBe(3);
        });

        it('should handle both coma and new line delimiters in a single string', function() {
            const result = StringCalculator.add('1,2\n3');

            expect(result).toBe(6);
        });
    });

    describe('Step 4', function() {
        it('should support custom delimiter', function() {
            const result = StringCalculator.add('//;\n1;2');

            expect(result).toBe(3);
        });

        it('should support special regexp chars as custom delimiter', function() {
            const result = StringCalculator.add('//?\n1?2');

            expect(result).toBe(3);
        });
    });

    describe('Step 5', function() {
        it('should throw an error on negative number', function() {
            expect(() => {
                StringCalculator.add('1,-2')
            }).toThrow('negatives not allowed: -2');
        });

        it('should throw an error on multiple negative numbers', function() {
            expect(() => {
                StringCalculator.add('-1,-2')
            }).toThrow('negatives not allowed: -1, -2');
        });
    });

    describe('Step 6', function() {
        it('should filter out numbers bigger then 1000', function() {
            const result = StringCalculator.add('1,1001,2');

            expect(result).toBe(3);
        });
    });

    describe('Step 7', function() {
        it('should support multi-char delimiters', function() {
            const result = StringCalculator.add('//[***]\n1***2');

            expect(result).toBe(3);
        });
    });

    describe('Step 8', function() {
        it('should support multiple delimiters', function() {
            const result = StringCalculator.add('//[*][%]\n1*2%3');

            expect(result).toBe(6);
        });
    });

    describe('Step 9', function() {
        it('should support multiple multi-char delimiters', function() {
            const result = StringCalculator.add('//[***][%?]\n1***2%?3');

            expect(result).toBe(6);
        });
    });
});
